## Build

1. Run `ng build` to build the "FrontSide" project. The build artifacts will be stored in the `dist/backSide/video-chat/public/` directory.
Use the `--prod` flag for a production build.

Run `npm start` in `dist/backSide/video-chat` directory to run server + new build front on `localhost:3000`

2. old front side (video) in `dist/backSide/public_old` copy all files into empty `dist/backSide/video-chat/public/`

& run `npm start` in `dist/backSide/video-chat` directory to run server + old front on `localhost:3000`


